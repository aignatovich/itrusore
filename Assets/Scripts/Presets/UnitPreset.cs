﻿using UnityEngine;

public class UnitPreset
{
    public string DefinitionId { get; private set; }
    public Vector3 Position { get; private set; }
    public float Rotation { get; private set; }
    public int Level { get; private set; }

    public UnitPreset( string definitionId, Vector3 position, float rotation, int level )
    {
        DefinitionId = definitionId;
        Position = position;
        Rotation = rotation;
        Level = level;
    }
}
