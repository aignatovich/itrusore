﻿using UnityEngine;
using System.Collections.Generic;

public class Presets
{
    ///какбы потом здесь будет грузиться джейсонина с расстановкой юнитов
    public ScenePreset LoadScenePreset( string presetId )
    {
        List<UnitPreset> unitsPresets = new List<UnitPreset>();
        for ( int i = 0; i < 16; ++i )
        {
            unitsPresets.Add( new UnitPreset( Constants.InfantryDefId, new Vector3( Random.Range( -20.0f, 20.0f ), 0, Random.Range( -10.0f, 10.0f ) ), Random.Range( 0f, 360.0f ), Random.Range( 1, 5 ) ) );
        }

        PlayerPreset playerPreset = new PlayerPreset( new Vector3( 0, 4, -35 ), Quaternion.identity );
        return new ScenePreset( presetId, unitsPresets, playerPreset );
    }
}
