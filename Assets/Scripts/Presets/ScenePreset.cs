﻿using System.Collections.Generic;

public class ScenePreset : BasePreset
{
    public List<UnitPreset> UnitsPresets { get; private set; }
    public PlayerPreset PlayerPreset { get; private set; }

    public ScenePreset( string presetId, List<UnitPreset> unitsPresets, PlayerPreset playerPreset ) : base( presetId )
    {
        this.UnitsPresets = unitsPresets;
        this.PlayerPreset = playerPreset;
    }
}
