﻿using UnityEngine;

public class PlayerPreset 
{
    public Vector3 StartPlayerPosition { get; private set; }
    public Quaternion StartPlayerRotation { get; private set; }

    public PlayerPreset( Vector3 startPlayerPosition, Quaternion startPlayerRotation )
    {
        StartPlayerPosition = startPlayerPosition;
        StartPlayerRotation = startPlayerRotation;
    }
}
