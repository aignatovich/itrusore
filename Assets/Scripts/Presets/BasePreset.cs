﻿using UnityEngine;
using System.Collections;

public class BasePreset 
{
    public string PresetId { get; private set; }

    public BasePreset( string presetId )
    {
        PresetId = presetId;
    }
}
