﻿using UnityEngine;

public class GuiManager
{
    private BaseScreen currenScreen;

    private const string uiRootPath = "Prefabs/GUI/UI Root";
    private GameObject uiRoot;

    public void Init()
    {
        var uiRootObject = Resources.Load<GameObject>( uiRootPath );
        uiRoot = GameObject.Instantiate( uiRootObject ) as GameObject;
    }

    public void ShowScreen<T>( string pathToScreen ) where T : BaseScreen
    {
        if ( currenScreen != null )
        {
            currenScreen.Close();
        }

        var newScreenObject = Resources.Load<GameObject>( pathToScreen );
        currenScreen = ( GameObject.Instantiate( newScreenObject ) as GameObject ).GetComponent<T>();
        GuiUtils.AddChild( uiRoot, currenScreen.gameObject );
        currenScreen.Init();
    }
}
