﻿using UnityEngine;

public class BaseScreen : MonoBehaviour {

    public virtual void Init()
    {
		// remark
    }

    public virtual void Close()
    {
        
    }
}
