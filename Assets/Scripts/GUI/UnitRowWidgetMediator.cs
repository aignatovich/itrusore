﻿using UnityEngine;

public class UnitRowWidgetMediator : MonoBehaviour
{
    [SerializeField] private UILabel unitNameLabel;

    public void Init( string unitName )
    {
        unitNameLabel.text = unitName;
    }
}
