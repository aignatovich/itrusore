﻿using UnityEngine;
using System.Collections.Generic;

public class MainScreen : BaseScreen
{
    [SerializeField] private UIGrid unitRowsGrid;

    private Dictionary<string, UnitRowWidgetMediator> unitRows;

    private const string unitRowPrefabPath = "Prefabs/GUI/UnitRowWidget";

    public override void Init()
    {
        base.Init();
        unitRows = new Dictionary<string, UnitRowWidgetMediator>();
        ApplicationManager.Instance.GameModel.UnitsDictionary.OnItemAddedOnContextEvent += OnUnitAdded;
        ApplicationManager.Instance.GameModel.UnitsDictionary.OnItemRemovedOnContextEvent += OnUnitRemoved;

        var unitRowPrefab = Resources.Load<GameObject>( unitRowPrefabPath );
        foreach ( var unitModel in ApplicationManager.Instance.GameModel.UnitsDictionary )
        {
            var unitRow = ( GameObject.Instantiate( unitRowPrefab ) as GameObject ).GetComponent<UnitRowWidgetMediator>();
            GuiUtils.AddChild( unitRowsGrid.gameObject, unitRow.gameObject );
            unitRow.Init( ApplicationManager.Instance.Definitions.UnitsDictionary[unitModel.Value.DefinitionId].Name );
            unitRows.Add( unitModel.Value.ModelId, unitRow );
        }

        unitRowsGrid.Reposition();
    }

    private void OnUnitRemoved( object sender, DictionaryAdapter<UnitModel>.ItemRemovedOnContextEventArgs<UnitModel> arg )
    {
        ApplicationManager.Instance.PoolManager.Despawn( unitRowPrefabPath, unitRows[arg.Item.ModelId].gameObject );
        unitRows.Remove( arg.Item.ModelId );
        unitRowsGrid.Reposition();
    }

    private void OnUnitAdded( object sender, DictionaryAdapter<UnitModel>.ItemAddedOnContextEventArgs<UnitModel> arg )
    {
        var unitRow = ApplicationManager.Instance.PoolManager.Spawn<UnitRowWidgetMediator>( unitRowPrefabPath, Vector3.zero, Quaternion.identity );
        GuiUtils.AddChild( unitRowsGrid.gameObject, unitRow.gameObject );
        unitRow.Init( ApplicationManager.Instance.Definitions.UnitsDictionary[arg.Item.DefinitionId].Name );
        unitRows.Add( arg.Item.ModelId, unitRow );
        unitRowsGrid.Reposition();
    }

    void OnDestroy()
    {
        ApplicationManager.Instance.GameModel.UnitsDictionary.OnItemAddedOnContextEvent -= OnUnitAdded;
        ApplicationManager.Instance.GameModel.UnitsDictionary.OnItemRemovedOnContextEvent -= OnUnitRemoved;
    }
}
