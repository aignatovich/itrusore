﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CachedBehaviour
{
    [SerializeField] private Camera playerCamera;

    //ToDo вынести эти параметры в дефы
    [SerializeField] private float minimumY = -60F;
    [SerializeField] private float maximumY = 60F;

    [SerializeField] private float sensitivityX = 15F;
    [SerializeField] private float sensitivityY = 15F;

    private float rotationY = 0F;
    private Vector3 movement = new Vector3();

    private Dictionary<string, TimeCounter> rechargeCounters;

    public void Init()
    {
        //ToDo AttackSpeed перенести в дефы прожектайлов/оружия
        rechargeCounters = new Dictionary<string, TimeCounter>();

        rechargeCounters[ApplicationManager.Instance.GameModel.playerModel.PrimaryWeapon] = new TimeCounter( ApplicationManager.Instance.Definitions.PlayerDefinitionDef.AttackSpeed );
        rechargeCounters[ApplicationManager.Instance.GameModel.playerModel.AlternativeWeapon] = new TimeCounter( ApplicationManager.Instance.Definitions.PlayerDefinitionDef.AttackSpeed );
    }

    private void Update()
    {
        float rotationX = CachedTransform.localEulerAngles.y + Input.GetAxis( "Mouse X" ) * sensitivityX;
        rotationY += Input.GetAxis( "Mouse Y" ) * sensitivityY;
        rotationY = Mathf.Clamp( rotationY, minimumY, maximumY );
        CachedTransform.localEulerAngles = new Vector3( -rotationY, rotationX, 0 );
        movement.x = Input.GetAxisRaw( "HorizontalMove" ) * ApplicationManager.Instance.Definitions.PlayerDefinitionDef.Speed;
        movement.z = Input.GetAxisRaw( "VerticalMove" ) * ApplicationManager.Instance.Definitions.PlayerDefinitionDef.Speed;
        CachedTransform.Translate( movement );

        foreach ( var counter in rechargeCounters )
        {
            counter.Value.Elapse( Time.deltaTime );
        }

        if ( Input.GetAxisRaw( "Fire1" ) != 0 )
        {
            TryAttack( ApplicationManager.Instance.GameModel.playerModel.PrimaryWeapon );
        }

        if ( Input.GetAxisRaw( "Fire2" ) != 0 )
        {
            TryAttack( ApplicationManager.Instance.GameModel.playerModel.AlternativeWeapon );
        }
    }

    private void TryAttack( string weaponId )
    {
        if ( rechargeCounters[weaponId].IsElapsed )
        {
            rechargeCounters[weaponId].Reset();
            ApplicationManager.Instance.ProjectileManager.LaunchProjectile( CachedTransform.position, CachedTransform.forward, weaponId );
        }
    }
}