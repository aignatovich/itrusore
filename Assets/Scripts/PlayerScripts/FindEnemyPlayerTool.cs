﻿using UnityEngine;


public class FindEnemyPlayerTool : MonoBehaviour 
{
    private string watchedUnitModelId;

    private void Update()
    {
        Ray ray = new Ray( transform.position, transform.forward );
        RaycastHit hit = new RaycastHit();
        Physics.Raycast( ray, out hit, 1000, LayerMask.GetMask( "Enemy" ) );

        if ( hit.collider == null )
        {
            watchedUnitModelId = "";
            ApplicationManager.Instance.UnitsManager.HideUnitHeader();
            return;
        }

        string newWatchedModelId = hit.collider.gameObject.GetComponent<UnitBehavior>().ModelId;

        if ( newWatchedModelId == watchedUnitModelId )
        {
            return;
        }

        watchedUnitModelId = newWatchedModelId;
        ApplicationManager.Instance.UnitsManager.ShowUnitHeader( newWatchedModelId );
    }
}
