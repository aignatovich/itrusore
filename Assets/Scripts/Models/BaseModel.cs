﻿public class BaseModel
{
    public string ModelId { get; private set; }

    public BaseModel( string modelId )
    {
        ModelId = modelId;
    }
}
