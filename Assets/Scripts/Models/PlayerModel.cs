﻿using UnityEngine;

public class PlayerModel : BaseModel
{
    public string DefinitionId { get; set; }
    /// <summary>
    /// пока прожектайлы и оружие - это одно и тоже
    /// </summary>
    public string PrimaryWeapon { get; set; }
    /// <summary>
    /// пока прожектайлы и оружие - это одно и тоже
    /// </summary>
    public string AlternativeWeapon { get; set; }
    public int Level { get; set; }
    public Vector3 Position { get; set; }
    public Quaternion Rotation { get; set; }

    public PlayerModel( string modelId, string definitionId, Vector3 position, Quaternion rotation, int level, string primaryWeapon, string alternativeWeapon ) : base( modelId )
    {
        DefinitionId = definitionId;
        Level = level;
        Position = position;
        Rotation = rotation;////12345677890
        PrimaryWeapon = primaryWeapon;
        AlternativeWeapon = alternativeWeapon;
    }
}
