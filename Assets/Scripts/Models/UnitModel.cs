﻿using UnityEngine;

public class UnitModel : BaseModel
{
    public string DefinitionId { get; private set; }

    public ExtendedInteger Hp { get; set; }
    public Vector3 Position { get; set; }
    public float Rotation { get; set; }
    public int Level { get; set; }

    //Так как требуются рандомные параметры - то рандомные  скорость и атака хранятся в модели
    //границы рандома всёравно хранятся в справочниках
    public float Speed { get; set; }
    public float Attack { get; set; }

    public UnitModel( string modelId, string definitionId, int level, Vector3 position, float rotation, int hp, float attack, float speed ) : base( modelId )
    {
        DefinitionId = definitionId;
        Speed = speed;
        Attack = attack;
        Hp = hp;
        Position = position;
        Rotation = rotation;
        Level = level;
    }
}
