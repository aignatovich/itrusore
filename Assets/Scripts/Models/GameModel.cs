﻿using System.Collections.Generic;

public class GameModel
{
    public DictionaryAdapter<UnitModel> UnitsDictionary { get; set; }
    public PlayerModel playerModel { get; set; }
    public string CurrentSceneId { get; set; } // по айди сцены надо брать пресеты, ну и саму сцену грузить

    //начально значение надо переделать
    private int lastModelId = 1000;
    //показывает, что это новый уровень, а не сохранённый
    private bool IsNewLevel = true;

    ///этот метод загружает модель, пока она создаётся, но потом должна грузиться из файлика
    public void InitModels()
    {
        if ( !IsNewLevel )
        {
            //типа загрузили сейв    
        }

        CurrentSceneId = "Scene_1";
        //новый уровень, поэтому создаём модель из пресета (стартовые позиции юнитов и т.д),         
        Presets preset = new Presets();
        ScenePreset scenePreset = preset.LoadScenePreset( CurrentSceneId );
        playerModel = new PlayerModel( GetNextModelId( "Player_" ), Constants.PlayerDefId, scenePreset.PlayerPreset.StartPlayerPosition, scenePreset.PlayerPreset.StartPlayerRotation, 1, "Def_KilledProjectile", "Def_SpawnedProjectile" );
        LoadNewUnitsInModel( scenePreset.UnitsPresets );
    }

    public void LoadNewUnitsInModel( List<UnitPreset> unitsPresets )
    {
        UnitsDictionary = new DictionaryAdapter<UnitModel>();

        string modelId;
        float speed;
        float attack;
        int hp;
        UnitLevelDefinition unitLevelDefinition;

        //создание из расстановок сцены новой модели
        foreach ( var unitPreset in unitsPresets )
        {
            modelId = GetNextModelId( Constants.UnitModelIdPrefix );
            unitLevelDefinition = ApplicationManager.Instance.Definitions.UnitLevelsDictionary[unitPreset.DefinitionId][unitPreset.Level];
            speed = UnityEngine.Random.Range( unitLevelDefinition.Speed.x, unitLevelDefinition.Speed.x );
            attack = UnityEngine.Random.Range( unitLevelDefinition.Attack.x, unitLevelDefinition.Attack.x );
            hp = (int) UnityEngine.Random.Range( unitLevelDefinition.Hp.x, unitLevelDefinition.Hp.x );
            UnitsDictionary.Add( modelId, new UnitModel( modelId, unitPreset.DefinitionId, unitPreset.Level, unitPreset.Position, unitPreset.Rotation, hp, attack, speed ) );
        }
    }

    public string GetNextModelId( string idPrefix )
    {
        return idPrefix + ++lastModelId;
    }
}
