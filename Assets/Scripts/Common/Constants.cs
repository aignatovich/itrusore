﻿public static class Constants
{
    //Временно айдишники похраним здесь, в будущем, в константах айдишников быть не должно
    //да и все константы должны быть перенесены в дефинишины приложения
    public const string InfantryDefId = "Def_InfantryUnit";
    public const string UnitModelIdPrefix = "Unit_";
    public const string PlayerDefId = "Def_Player";
}
