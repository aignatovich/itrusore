﻿using UnityEngine;

public class UnitHeader : MonoBehaviour
{
    [SerializeField] private TextMesh nameLabel;
    [SerializeField] private TextMesh attackLabel;
    [SerializeField] private TextMesh speedLabel;

    private string attackFormat = "Attack {0}";
    private string speedFormat = "Speed {0}";

    public void Init( string nmae, float attackValue, float speedValue)
    {
        nameLabel.text = nmae;
        attackLabel.text = string.Format( attackFormat, attackValue );
        speedLabel.text = string.Format( speedFormat, speedValue );
    }

    void Update()
    {
        transform.LookAt( Camera.main.transform );
    }
}
