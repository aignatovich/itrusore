﻿using System.Collections.Generic;
using System;

public class DictionaryAdapter<T>
{
    public event EventHandler<ItemAddedOnContextEventArgs<T>> OnItemAddedOnContextEvent;
    public event EventHandler<ItemRemovedOnContextEventArgs<T>> OnItemRemovedOnContextEvent;

    public Dictionary<string, T> dictionary { get; set; }

    public bool IsEmpty
    {
        get { return dictionary.Count == 0; }
    }

    public DictionaryAdapter()
    {
        dictionary = new Dictionary<string, T>();
    }

    public DictionaryAdapter( Dictionary<string, T> _dictionary )
    {
        dictionary = _dictionary;
    }

    public void Add( string key, T item )
    {
        dictionary.Add( key, item );
        if ( OnItemAddedOnContextEvent != null )
        {
            OnItemAddedOnContextEvent( this, new ItemAddedOnContextEventArgs<T>( key, item ) );
        }
    }

    public void Remove( string key )
    {
        T item = this[key];

        dictionary.Remove( key );

        if ( OnItemRemovedOnContextEvent != null )
        {
            OnItemRemovedOnContextEvent( this, new ItemRemovedOnContextEventArgs<T>( key, item ) );
        }
    }

    public void Clear()
    {
        dictionary.Clear();
    }

    public bool ContainsKey( string key )
    {
        return dictionary.ContainsKey( key );
    }

    public bool ContainsValue( T value )
    {
        return dictionary.ContainsValue( value );
    }

    public bool TryGetValue( string key, out T value )
    {
        return dictionary.TryGetValue( key, out value );
    }

    public IEnumerator<KeyValuePair<string, T>> GetEnumerator()
    {
        return dictionary.GetEnumerator();
    }

    public T this[ string key ]
    {
        get { return dictionary[key]; }

        set { dictionary[key] = value; }
    }

    public class ItemAddedOnContextEventArgs<S> : EventArgs
    {
        public string Key { get; private set; }
        public S Item { get; private set; }

        public ItemAddedOnContextEventArgs( string key, S item )
        {
            Key = key;
            Item = item;
        }
    }

    public class ItemRemovedOnContextEventArgs<S> : EventArgs
    {
        public string Key { get; private set; }
        public S Item { get; private set; }

        public ItemRemovedOnContextEventArgs( string key, S item )
        {
            Key = key;
            Item = item;
        }
    }
}
