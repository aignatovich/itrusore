﻿using System;

public class ExtendedInteger
{
    public Action<ExtendedIntegerEventArgs> Changed;

    private int currentValue;

    public int Value
    {
        get
        {
            return currentValue;
        }

        set
        {
            int oldValue = currentValue;
            currentValue = value;
            OnChanged( new ExtendedIntegerEventArgs( oldValue, currentValue ) );
        }
    }

    public ExtendedInteger( int value )
    {
        Value = value;
    }

    public static implicit operator ExtendedInteger( int value )
    {
        return new ExtendedInteger( value );
    }

    public static implicit operator int( ExtendedInteger currentInstance )
    {
        return currentInstance.Value;
    }

    protected virtual void OnChanged( ExtendedIntegerEventArgs e )
    {
        if ( Changed != null )
        {
            Changed( e );
        }
    }

    #region EventArgs

    public class ExtendedIntegerEventArgs : EventArgs
    {
        public int OldValue { get; private set; }
        public int NewValue { get; private set; }

        public ExtendedIntegerEventArgs( int oldValue, int newValue )
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
    }

    #endregion
}