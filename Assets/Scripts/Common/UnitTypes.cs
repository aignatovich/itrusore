﻿public enum UnitType
{
    Light,
    Medium,
    Heavy,
    Special
}
