﻿public class ProjectileDefinition : BaseDefinition
{
    public string AttackLayerName { get; private set; }
    public string ActionName { get; private set; }

    public ProjectileDefinition( string definitionId, string actionName, string attackLayerName ) : base( definitionId )
    {
        AttackLayerName = attackLayerName;
        ActionName = actionName;
    }
}
