﻿using UnityEngine;
using System.Collections;

public class UnitDefinition : BaseDefinition
{
    public string Name { get; private set; }
    public UnitType UnitType { get; private set; }

    public UnitDefinition( string definitionId, string name, UnitType unitType ) : base( definitionId )
    {
        Name = name;
        UnitType = unitType;
    }
}
