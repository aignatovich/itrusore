﻿public class BaseLevelDefinition
{
    public int Level { get; private set; }

    public BaseLevelDefinition(int level)
    {
        Level = level;
    }
}
