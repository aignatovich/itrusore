﻿public class PlayerDefinition : BaseDefinition
{
    public float Speed { get; private set; }
    public float Attack { get; private set; }
    public int Hp { get; private set; }
    public string PrefabPath { get; private set; }
    public float AttackSpeed { get; private set; }

    public PlayerDefinition( string definitionId, string prefabPath, float speed, float attack, int hp, float attackSpeed ) : base( definitionId )
    {
        Speed = speed;
        Attack = attack;
        Hp = hp;
        PrefabPath = prefabPath;
        AttackSpeed = attackSpeed;
    }
}
