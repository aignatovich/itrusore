﻿using UnityEngine;

public class UnitLevelDefinition: BaseLevelDefinition
{
    //Vector2 используется для для указания границ параметров, чтобы рандомом виз этого промежутка взять параметр в модель
    public Vector2 Speed { get; private set; }
    public Vector2 Attack { get; private set; }
    public Vector2 Hp { get; private set; }
    public string VisualPrefabPath { get; private set; }

    public UnitLevelDefinition(int level, Vector2 speed, Vector2 attack, Vector2 hp, string visualPrefabPath): base(level)
    {
        Speed = speed;
        Attack = attack;
        Hp = hp;
        VisualPrefabPath = visualPrefabPath;
    }
}
