﻿public class ApplicationSettingsDefinition : BaseDefinition
{
    public int StartUnitsValue { get; private set; }

    public ApplicationSettingsDefinition( string definitionId, int startUnitsValue ) : base( definitionId )
    {
        StartUnitsValue = startUnitsValue;
    }
}
