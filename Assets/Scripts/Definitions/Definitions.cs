﻿using UnityEngine;
using System.Collections.Generic;

public class Definitions
{
    public ApplicationSettingsDefinition ApplicationSettingsDefinitionDef;
    public Dictionary<string, ProjectileDefinition> ProjectilesDictionary;
    public Dictionary<string, UnitDefinition> UnitsDictionary;
    public Dictionary<string, Dictionary<int, UnitLevelDefinition>> UnitLevelsDictionary;
    public PlayerDefinition PlayerDefinitionDef;

    //потом надо както из джейсона справочники грузить
    public void InitDefinitions()
    {
        ApplicationSettingsDefinitionDef = new ApplicationSettingsDefinition( "Def_ApplicationSettings", 15 );
        UnitsDictionary = new Dictionary<string, UnitDefinition>();
        UnitsDictionary.Add( Constants.InfantryDefId, new UnitDefinition( Constants.InfantryDefId, "Base unit", UnitType.Medium ) );

        UnitLevelsDictionary = new Dictionary<string, Dictionary<int, UnitLevelDefinition>>();
        var levels = new Dictionary<int, UnitLevelDefinition>();
        levels.Add( 1, new UnitLevelDefinition( 1, new Vector2( 10, 12 ), new Vector2( 20, 22 ), new Vector2( 30, 33 ), "Prefabs/Units/FirstUnit" ) );
        levels.Add( 2, new UnitLevelDefinition( 2, new Vector2( 13, 15 ), new Vector2( 23, 25 ), new Vector2( 40, 45 ), "Prefabs/Units/FirstUnit" ) );
        levels.Add( 3, new UnitLevelDefinition( 3, new Vector2( 17, 20 ), new Vector2( 26, 30 ), new Vector2( 50, 55 ), "Prefabs/Units/FirstUnit" ) );
        levels.Add( 4, new UnitLevelDefinition( 4, new Vector2( 20, 21 ), new Vector2( 31, 35 ), new Vector2( 60, 65 ), "Prefabs/Units/FirstUnit" ) );
        levels.Add( 5, new UnitLevelDefinition( 5, new Vector2( 22, 23 ), new Vector2( 37, 40 ), new Vector2( 70, 80 ), "Prefabs/Units/FirstUnit" ) );

        ProjectilesDictionary = new Dictionary<string, ProjectileDefinition>();
        ProjectilesDictionary.Add( "Def_SpawnedProjectile", new ProjectileDefinition( "Def_SpawnedProjectile", "SpawnEnemy", "Ground" ) );
        ProjectilesDictionary.Add( "Def_KilledProjectile", new ProjectileDefinition( "Def_KilledProjectile", "KillEnemy", "Enemy" ) );

        UnitLevelsDictionary.Add( Constants.InfantryDefId, levels );
        PlayerDefinitionDef = new PlayerDefinition( "Def_Player", "Prefabs/Players/Player", 0.2f, 10, 10, 1 );
    }
}
