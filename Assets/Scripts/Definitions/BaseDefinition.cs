﻿public class BaseDefinition
{
    public string DefinitionId { get; private set; }

    public BaseDefinition( string definitionId )
    {
        DefinitionId = definitionId;
    }
}
