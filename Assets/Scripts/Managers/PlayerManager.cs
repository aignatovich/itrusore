﻿using UnityEngine;
using System.Collections;

public class PlayerManager
{
    private PlayerController playrController;

    public void Init()
    {
        InitPlayer();
    }

    private void InitPlayer()
    {
        playrController = ( GameObject.Instantiate( Resources.Load<GameObject>( ApplicationManager.Instance.Definitions.PlayerDefinitionDef.PrefabPath ) ) as GameObject ).GetComponent<PlayerController>();
        playrController.Init();
    }
}
