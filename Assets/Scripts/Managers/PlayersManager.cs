﻿using UnityEngine;

public class PlayersManager
{
    private PlayerController playrController;

    public void Init()
    {
        InitPlayer();
    }

    private void InitPlayer()
    {
        playrController = ( GameObject.Instantiate( Resources.Load<GameObject>( ApplicationManager.Instance.Definitions.PlayerDefinitionDef.PrefabPath ) ) as GameObject ).GetComponent<PlayerController>();

        playrController.CachedTransform.localRotation = ApplicationManager.Instance.GameModel.playerModel.Rotation;
        playrController.CachedTransform.localPosition = ApplicationManager.Instance.GameModel.playerModel.Position;
        playrController.Init();
    }
}
