﻿using System.Runtime.CompilerServices;
using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    public Definitions Definitions{ get; private set; }
    public GameModel GameModel { get; private set; }

    public UnitsManager UnitsManager { get; private set; }
    public PlayersManager PlayersManager { get; private set; }
    public PoolManager PoolManager { get; private set; }
    public GuiManager GuiManager { get; private set; }
    public ProjectileManager ProjectileManager { get; private set; }

    public static ApplicationManager Instance { get { return instance; } }

    private static ApplicationManager instance;
    private const string mainScreenPrefabPath = "Prefabs/GUI/MainScreen";

    private ApplicationManager()
    {
        instance = this;
    }

    void Start()
    {
        Definitions = new Definitions();
        Definitions.InitDefinitions();

        GameModel = new GameModel();
        GameModel.InitModels();
        
        PoolManager = new PoolManager();
        PoolManager.Init();

        UnitsManager = new UnitsManager();
        UnitsManager.Init();

        PlayersManager = new PlayersManager();
        PlayersManager.Init();

        ProjectileManager = new ProjectileManager();
        ProjectileManager.Init();

        GuiManager = new GuiManager();
        GuiManager.Init();
        GuiManager.ShowScreen<MainScreen>( mainScreenPrefabPath );

    }
}
