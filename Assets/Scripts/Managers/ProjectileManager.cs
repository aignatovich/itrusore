﻿using System.Linq;
using UnityEngine;

//потом, когда игрок будет стрелять разными штуками. и юниты тоже, этот менеджер должен будет управлять всеми этими штуками(прожектайлами)
public class ProjectileManager
{
    public void Init()
    {

    }

    //Todo сделать нормальные прожектайлы, или хотябы не такие кривые как сейчас
    public void LaunchProjectile( Vector3 launchPosition, Vector3 direction, string projectileDefId )
    {
        LayerMask attackedObjectsMask = LayerMask.GetMask( ApplicationManager.Instance.Definitions.ProjectilesDictionary[projectileDefId].AttackLayerName );
        
        Ray ray = new Ray( launchPosition, direction );
        RaycastHit hit = new RaycastHit();
        Physics.Raycast( ray, out hit, 1000, attackedObjectsMask );
        if ( hit.collider != null )
        {
            switch (ApplicationManager.Instance.Definitions.ProjectilesDictionary[projectileDefId].DefinitionId)
            {
                case "Def_SpawnedProjectile":
                    ApplicationManager.Instance.UnitsManager.CreateNewUnit( hit.point, ApplicationManager.Instance.Definitions.UnitsDictionary.Values.First().DefinitionId, UnityEngine.Random.Range( 1, 5 ) );
                    break;
                case "Def_KilledProjectile":
                    var killedModelId = hit.collider.gameObject.GetComponent<UnitBehavior>().ModelId;
                    ApplicationManager.Instance.GameModel.UnitsDictionary[killedModelId].Hp.Value = 0;

                    break;
            }
        }
    }
}
