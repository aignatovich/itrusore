﻿using System.Collections.Generic;
using UnityEngine;

public class UnitsManager
{
    public Dictionary<string, UnitBehavior> Units { get; private set; }

    private GameObject unitsContainer;
    private UnitHeader unitHeader;

    private const string headerPrefabPath = "Prefabs/Common/UnitHeader";

    public void Init()
    {
        unitsContainer = new GameObject( "units" );
        //небольшой префабный пулл, вдруг надо при старте создать много юниты с одинаковыми префабами при ините сцены
        Dictionary<string, GameObject> loadedUnitsPrefabs = new Dictionary<string, GameObject>();

        Units = new Dictionary<string, UnitBehavior>();
        foreach ( var unitModel in ApplicationManager.Instance.GameModel.UnitsDictionary.dictionary )
        {
            GameObject unitVisualModelPrefab = null;
            var unitLevelDefinition = ApplicationManager.Instance.Definitions.UnitLevelsDictionary[unitModel.Value.DefinitionId][unitModel.Value.Level];

            if ( loadedUnitsPrefabs.ContainsKey( unitLevelDefinition.VisualPrefabPath ) )
            {
                unitVisualModelPrefab = loadedUnitsPrefabs[unitLevelDefinition.VisualPrefabPath];
            }
            else
            {
                unitVisualModelPrefab = Resources.Load<GameObject>( unitLevelDefinition.VisualPrefabPath );
            }

            if ( !loadedUnitsPrefabs.ContainsKey( unitLevelDefinition.VisualPrefabPath ) )
            {
                loadedUnitsPrefabs.Add( unitLevelDefinition.VisualPrefabPath, unitVisualModelPrefab );
            }

            var unit = ( GameObject.Instantiate( unitVisualModelPrefab, unitModel.Value.Position, new Quaternion( 0, unitModel.Value.Rotation, 0, 0 ) ) as GameObject ).AddComponent<UnitBehavior>();
            unit.gameObject.transform.parent = unitsContainer.transform;
            unit.Init( unitModel.Value.ModelId );
            Units.Add( unitModel.Value.ModelId, unit );

            //Костыль
            unit.CachedTransform.Translate( 0, unit.transform.localScale.y / 2, 0 );
        }

        //подписаться  на изменение модели

        ApplicationManager.Instance.GameModel.UnitsDictionary.OnItemAddedOnContextEvent += OnUnitAdded;
        ApplicationManager.Instance.GameModel.UnitsDictionary.OnItemRemovedOnContextEvent += OnUnitRemoved;
    }

    private void OnUnitRemoved( object sender, DictionaryAdapter<UnitModel>.ItemRemovedOnContextEventArgs<UnitModel> e )
    {
        if ( Units.ContainsKey( e.Item.ModelId ) )
        {
            ApplicationManager.Instance.PoolManager.Despawn( ApplicationManager.Instance.Definitions.UnitLevelsDictionary[e.Item.DefinitionId][e.Item.Level].VisualPrefabPath, Units[e.Item.ModelId].gameObject );
            Units.Remove( e.Item.ModelId );
        }
    }

    private void OnUnitAdded( object sender, DictionaryAdapter<UnitModel>.ItemAddedOnContextEventArgs<UnitModel> e )
    {
        if ( !Units.ContainsKey( e.Item.ModelId ) )
        {
            var unit = ApplicationManager.Instance.PoolManager.Spawn<UnitBehavior>( ApplicationManager.Instance.Definitions.UnitLevelsDictionary[e.Item.DefinitionId][e.Item.Level].VisualPrefabPath, e.Item.Position, new Quaternion( 0, e.Item.Rotation, 0, 0 ) );
            unit.Init( e.Item.ModelId );
            unit.CachedTransform.parent = unitsContainer.transform;
            Units.Add( e.Item.ModelId, unit );

            //Костыль
            unit.CachedTransform.Translate( 0, unit.transform.localScale.y / 2, 0 );
        }
    }

    public void ShowUnitHeader( string UnitModelId )
    {
        if ( unitHeader == null )
            unitHeader = ApplicationManager.Instance.PoolManager.Spawn<UnitHeader>( headerPrefabPath, Units[UnitModelId].transform.position, Quaternion.identity );

        unitHeader.gameObject.transform.parent = Units[UnitModelId].gameObject.transform;
        ///снова костыль
        unitHeader.gameObject.transform.localPosition = new Vector3( 0, 3, 0 );
        unitHeader.gameObject.SetActive( true );

        var unitModel = ApplicationManager.Instance.GameModel.UnitsDictionary[UnitModelId];
        unitHeader.Init( ApplicationManager.Instance.Definitions.UnitsDictionary[unitModel.DefinitionId].Name, unitModel.Attack, unitModel.Speed );
    }

    public void HideUnitHeader()
    {
        if ( unitHeader == null )
            return;

        unitHeader.gameObject.SetActive( false );
        ApplicationManager.Instance.PoolManager.Despawn( headerPrefabPath, unitHeader.gameObject );
        unitHeader = null;
    }

    public void CreateNewUnit( Vector3 position, string definitionId, int level )
    {
        var newModelId = ApplicationManager.Instance.GameModel.GetNextModelId( Constants.UnitModelIdPrefix );
        var levelDefinition = ApplicationManager.Instance.Definitions.UnitLevelsDictionary[definitionId][level];

        var unitModel = new UnitModel(
            newModelId,
            definitionId,
            level,
            position,
            0,
            (int) Random.Range( levelDefinition.Hp.x, levelDefinition.Hp.y ),
            (int) Random.Range( levelDefinition.Attack.x, levelDefinition.Attack.y ),
            (int) Random.Range( levelDefinition.Speed.x, levelDefinition.Speed.y )
            );

        ApplicationManager.Instance.GameModel.UnitsDictionary.Add( newModelId, unitModel );
    }

    public void RemoveUnit( string unitModelId )
    {
        var definitionId = ApplicationManager.Instance.GameModel.UnitsDictionary[unitModelId].DefinitionId;
        var level = ApplicationManager.Instance.GameModel.UnitsDictionary[unitModelId].Level;
        ApplicationManager.Instance.PoolManager.Despawn( ApplicationManager.Instance.Definitions.UnitLevelsDictionary[definitionId][level].VisualPrefabPath, Units[unitModelId].gameObject );
        Units.Remove( unitModelId );
        ApplicationManager.Instance.GameModel.UnitsDictionary.Remove( unitModelId );
    }
}
