﻿using UnityEngine;

public class CachedBehaviour : MonoBehaviour
{
    private Collider cachedCollider;
    private Animation cachedAnimation;
    private Renderer cachedRenderer;
    private Rigidbody cachedRigidbody;
    private Transform cachedTransform;

    public Rigidbody CachedRigidbody
    {
        get
        {
            if ( cachedRigidbody == null )
                cachedRigidbody = rigidbody;

            return cachedRigidbody;
        }
    }

    public Collider CachedCollider
    {
        get
        {
            if ( cachedCollider == null )
                cachedCollider = collider;

            return cachedCollider;
        }
    }

    public Animation CachedAnimation
    {
        get
        {
            if ( cachedAnimation == null )
                cachedAnimation = GetComponent<Animation>();

            return cachedAnimation;
        }
    }

    public Renderer CachedRenderer
    {
        get
        {
            if ( cachedRenderer == null )
                cachedRenderer = renderer;

            return cachedRenderer;
        }
    }

    public Transform CachedTransform
    {
        get
        {
            if ( cachedTransform == null )
                cachedTransform = transform;

            return cachedTransform;
        }
    }
}