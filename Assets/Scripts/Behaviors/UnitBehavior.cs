﻿public class UnitBehavior : CachedBehaviour
{
    public string ModelId { get; private set; }

    public void Init( string modelId )
    {
        this.ModelId = modelId;
        ApplicationManager.Instance.GameModel.UnitsDictionary[modelId].Hp.Changed += OnHpChanged;
    }

    private void OnHpChanged( ExtendedInteger.ExtendedIntegerEventArgs extendedIntegerEventArgs )
    {
        if (extendedIntegerEventArgs.NewValue <= 0)
        {
            //анимации смерти и тд
            ApplicationManager.Instance.UnitsManager.RemoveUnit( ModelId );
        }
    }
}
